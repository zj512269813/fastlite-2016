/*
 * Barebones implementation of displaying camera preview.
 * 
 * Created by lisah0 on 2012-02-24
 */
package com.darryring.libview;

import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PreviewCallback;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

/**
 * 摄像头预览组件
 */
public class LibCameraPreview extends SurfaceView implements SurfaceHolder.Callback {
    String TAG="LibCameraPreview";
    private SurfaceHolder mHolder;
    private Camera mCamera;
    private PreviewCallback previewCallback;
    private AutoFocusCallback autoFocusCallback;

    public LibCameraPreview(Context context,
                            PreviewCallback previewCb,
                            AutoFocusCallback autoFocusCb) {
        super(context);
        previewCallback = previewCb;
        autoFocusCallback = autoFocusCb;
        /*
         * Set camera to continuous focus if supported, otherwise use
         * software auto-focus. Only works for API level >=9.
         */
        /*
        Camera.Parameters parameters = camera.getParameters();
        for (String f : parameters.getSupportedFocusModes()) {
            if (f == Parameters.FOCUS_MODE_CONTINUOUS_PICTURE) {
                mCamera.setFocusMode(Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                autoFocusCallback = null;
                break;
            }
        }
        */

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mHolder = getHolder();
        mHolder.addCallback(this);

        // deprecated setting, but required on Android versions prior to 3.0
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        Log.i(TAG,"surfaceCreated...");
        // The Surface has been created, now tell the camera where to draw the preview.
        try {
            if(mCamera==null){
                mCamera = getCameraInstance();
            }
            mCamera.setPreviewDisplay(holder);
        } catch (IOException e) {
            Log.d("DBG", "Error setting camera preview: " + e.getMessage());
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        // Camera preview released in activity
        Log.i(TAG,"surfaceDestroyed...");
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.i(TAG,"surfaceChanged...");
        /*
         * If your preview can change or rotate, take care of those events here.
         * Make sure to stop the preview before resizing or reformatting it.
         */
        if (mHolder.getSurface() == null){
          // preview surface does not exist
          return;
        }

        // stop preview before making changes
        try {
            mCamera.stopPreview();
        } catch (Exception e){
          // ignore: tried to stop a non-existent preview
        }

        try {
            // Hard code camera surface rotation 90 degs to match Activity view in portrait
            mCamera.setDisplayOrientation(90);

            mCamera.setPreviewDisplay(mHolder);
            mCamera.setPreviewCallback(previewCallback);
            mCamera.startPreview();
            mCamera.autoFocus(autoFocusCallback);
        } catch (Exception e){
            Log.d("DBG", "Error starting camera preview: " + e.getMessage());
        }
    }

    /**
     * A safe way to get an instance of the Camera object.
     */
    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open();
        } catch (Exception e) {
        }
        return c;
    }
    private boolean previewing;
    public void releaseCamera() {
        Log.i(TAG,"releaseCamera...");
        if (mCamera != null) {
            previewing = false;
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
        }
    }
    public boolean isPreviewing() {
        return previewing;
    }
    public void setPreviewing(boolean previewing) {
        this.previewing = previewing;
    }
    public void startPreview(){
        Log.i(TAG,"startPreview...");
        if(mCamera!=null){
            mCamera.startPreview();
        }
    }
    public void setPreviewCallback(PreviewCallback _call){
        Log.i(TAG,"setPreviewCallback...");
        if(mCamera!=null){
            mCamera.setPreviewCallback(_call);
        }
    }
    private Camera.PictureCallback mPictureCallback;

    /**
     * @param _call
     */
    public void setPictureCallback(Camera.PictureCallback _call){
        mPictureCallback = _call;

    }
    /**
     * 拍照
     */
    public void takePicture(){
        if(mCamera!=null){
            mCamera.takePicture(null,null,mPictureCallback);
        }
    }

    /**
     *
     */
    public void stopPreview(){
        Log.i(TAG,"stopPreview...");
        if(mCamera!=null){
            mCamera.stopPreview();
        }
    }
    public void autoFocus(AutoFocusCallback _call){
        Log.i(TAG,"autoFocus...");
        if(mCamera!=null) {
            mCamera.autoFocus(_call);
        }
    }
}
