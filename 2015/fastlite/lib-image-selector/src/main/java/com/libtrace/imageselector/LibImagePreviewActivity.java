package com.libtrace.imageselector;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.libtrace.imageselector.adapter.GestureSettingsMenu;
import com.libtrace.imageselector.adapter.PaintingsPagerAdapter;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;
import java.util.List;

/**
 * 图片预览页面，支持触控和双击放大缩小
 * Created by hljdrl on 16/1/6.
 */
public class LibImagePreviewActivity extends FragmentActivity  implements ViewPager.OnPageChangeListener {

    public static final String EXTRA_IMAGE_URI_LIST="IMAGE_URI_LIST";

    public static final String EXTRA_URI_TYPE="IMAGE_URI_TYPE";

    public static final String EXTRA_SHOW_URI="IMAGE_SHOW_URI";

    public static final String EXTRA_SHOW_SELECT = "IMAGE_SHOW_SELECT";

    public static final int URI_TYPE_FILE=1;

    public static final int URI_TYPE_HTTP=2;

    private List<String> imageUris = new ArrayList<String>();


    ViewPager mViewPager;

    private GestureSettingsMenu mSettingsMenu;

    TextView mTitle;
    private int imageUriType = URI_TYPE_FILE;

    TextView btn_image_uri;

    boolean showUri = Boolean.FALSE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(!ImageLoader.getInstance().isInited()){
            ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(this));
        }
        int _select = getIntent().getIntExtra(EXTRA_SHOW_SELECT,0);
        imageUriType = getIntent().getIntExtra(EXTRA_URI_TYPE,URI_TYPE_FILE);
        showUri = getIntent().getBooleanExtra(EXTRA_SHOW_URI,false);
        List<String> _uris = getIntent().getStringArrayListExtra(EXTRA_IMAGE_URI_LIST);
        if(_uris!=null){
            imageUris = _uris;
        }
        setContentView(R.layout.lib_image_activity_preview);
        mTitle = (TextView) findViewById(R.id.lib_image_btn_title);
        btn_image_uri = (TextView) findViewById(R.id.lib_image_uri);
        Button mSubmitButton = (Button) findViewById(R.id.commit);
        if(mSubmitButton!=null){
            mSubmitButton.setVisibility(View.INVISIBLE);
        }
        mSettingsMenu = new GestureSettingsMenu(this);
        mViewPager = (ViewPager) findViewById(R.id.paintings_view_pager);
        mViewPager.setAdapter(new PaintingsPagerAdapter(mViewPager, imageUris, mSettingsMenu,imageUriType));
        mViewPager.addOnPageChangeListener(this);
        if(_select!=0){
            onPageSelected(_select);
            updateTitle(_select,imageUris.size());
            mViewPager.setCurrentItem(_select);
        }else{
            onPageSelected(0);
            updateTitle(0,imageUris.size());
        }

        findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        mTitle.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        if(showUri){
            btn_image_uri.setVisibility(View.VISIBLE);
        }

    }
    void updateTitle(int index,int size){
        if(mTitle!=null){
            StringBuffer _buf = new StringBuffer();
            _buf.append((index+1));
            _buf.append("/");
            _buf.append(size);
            mTitle.setText(_buf.toString());
        }
        if(showUri){
            if(imageUris!=null){
                if(index<imageUris.size()) {
                    btn_image_uri.setText(imageUris.get(index));
                }
            }

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }
    @Override
    public void onPageSelected(int position) {
        updateTitle(position,imageUris.size());
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
