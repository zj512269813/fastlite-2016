package com.darryring.libcore.media;


/**
 * Created by hljdrl on 15/12/11.
 */
public interface Media<T,M> {


    public  M getPlayer();

    /**
     * @param _uri
     * @param video
     * @param screen
     */
    public void openMediaAsyn(String _uri,boolean video,T screen);

    /**
     *
     */
    public void pause();

    /**
     *
     */
    public void start();


    /**
     *
     */
    public void stop();

    /**
     *
     */
    public void release();

    public boolean isPlaying();

    public int getDuration();

    public int getCurrentPosition();

    public void seekTo(int msec);


    /**
     * @param _call
     */
    public void addMediaListener(MediaListener _call);

    /**
     * @param _call
     */
    public void removeMediaListener(MediaListener _call);


}
