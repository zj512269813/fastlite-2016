package com.darryring.libcore.task;


/**
 * Created by hljdrl on 15/12/11.
 */
public interface Task {

    /**
     * @param _run
     */
    public void postTask(Runnable _run);

    /**
     *
     */
    public void shutdownTask();
}
