package com.darryring.libchat;

import com.darryring.libchat.call.MessageCall;
import com.darryring.libchat.call.MessageListener;

import java.util.List;

/**
 * Created by hljdrl on 16/4/11.
 */
public interface ChatManager<T> {


    /**
     *
     */
    void load();

    /**
     *
     */
    void init();

    /**
     * @param chatUserID
     * @param group 群组聊天
     * @return
     */
    int getCacheMessaegCount(String chatUserID,boolean group);


    /**
     * @param chatUserID
     * @param startIndex
     * @param endIndex
     * @param group 群组聊天
     * @return
     */
    List<T> getCacheMessages(String chatUserID, int startIndex, int endIndex,boolean group);


    /**
     * 绑定一个聊天用户,意味着和这个用户存在一个聊天窗体
     * @param chatId
     * @param group 群聊
     */

    void bindOpenChatId(String chatId,boolean group);

    /**
     * 判断是否和当前用户存在打开的聊天窗体
     * @param chatId
     * @return
     */
    boolean isBindOpenChatId(String chatId);

    /**
     * 永久删除一条消息
     *
     * @param msg
     * @param _call
     */
    public void deleteMessage(T msg, MessageCall _call);


    /**
     * 更新一条消息的状态
     * @param msg
     * @param _call
     */
    public void updateMessageCache(T msg,MessageCall _call);

    /**
     * 个人聊天-发送文本
     *
     * @param msg
     */
    void sendChatMessageText(T msg, MessageCall _call);

    /**
     * 个人聊天-发送语音文件
     *
     * @param msg
     */
    void sendChatMessageVoice(T msg, MessageCall _call);

    /**
     * 个人聊天-发送图片文件
     *
     * @param msg
     */
    void sendChatMessageImage(T msg, MessageCall _call);

    /**
     * 注册消息监听接口
     *
     * @param _call
     */
    void addMessageListener(MessageListener _call);

    /**
     * 注销消息监听接口
     *
     * @param _call
     */
    void removeMessageListener(MessageListener _call);
}
