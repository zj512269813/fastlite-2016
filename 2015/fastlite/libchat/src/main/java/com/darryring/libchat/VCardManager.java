package com.darryring.libchat;

import com.darryring.libchat.call.OkCall;

import java.util.List;

/**
 *
 * VCardManger个人信息类
 * Created by hljdrl on 16/4/12.
 */
public interface VCardManager<T> {

    /**
     * 初始化方法
     */
    void init();
    /**
     * 获得当前用户的个人资料
     * @return
     */
    T getUserVCard();

    /**
     * 获得个人资料所有的
     * @return
     */
    List<T> getVCards();

    /**
     * 加载一个好友资料-本地缓存
     * @param userId
     */
    T laodLocalCacheVCard(String userId);

    /**
     * 删除一个VCard-本地缓存
     * @param userId
     */
    void removeLocalCacheVCard(String userId);

    /**
     * 加载所有好友资料-网络数据
     */
    void loadNetAllVCard();

    /**
     * 加载VCard数据,本地有缓存则从缓存读取,阻塞方法
     * @param sid
     * @return
     */
    T loadNetVCard(String sid);


    /**
     * 强制加载覆盖本地数据,个人详情页面强制更新数据
     * @param sid
     */
    T loadNetVCardForced(String sid);


    /**
     * 修改个人资料：昵称、手机号、个人简介
     * @param userName 昵称
     * @param phone 手机号
     * @param usersay 个人简介
     * @param _call 回调接口
     */
    @Deprecated
    void editPerson(String userName,String phone,String usersay,OkCall _call);

    /**
     * 修改个人资料：昵称、手机号、个人简介
     * @param userName 昵称
     * @param phone 手机号
     * @param usersay 个人简介
     * @param _call 回调接口
     */
    void editVCardPersonAsyn(String userName,String phone,String usersay,OkCall _call);

}
