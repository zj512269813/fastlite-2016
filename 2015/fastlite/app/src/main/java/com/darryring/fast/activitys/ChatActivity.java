package com.darryring.fast.activitys;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.darryring.fast.R;
import com.darryring.fast.base.BaseAppCompatActivity;
import com.darryring.fast.theme.FastTheme;
import com.darryring.libcore.util.StringUtil;
import com.darryring.libmodel.entity.theme.ThemeEntity;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class ChatActivity extends BaseAppCompatActivity {
    private static String CHAT_ID="chatID";
    private static String CHAT_NAME="chatName";
    public static void runIntentChat(Activity aty,String chatId,String name){
        Intent intent = new Intent(aty,ChatActivity.class);
        intent.putExtra(CHAT_ID,chatId);
        intent.putExtra(CHAT_NAME,name);
        aty.startActivity(intent);
    }


    private String TAG="ChatActivity";
//    ChatManager mChatManager;
    ListView mListView;
    EditText mEditText;
    Button mBtnSend;
    Button mBtnAudio;
    //
    String chatId;
    String chatName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FastTheme.fastTheme.setupTheme(this, ThemeEntity.TYPE_NO_ACTION_BAR);
        super.onCreate(savedInstanceState);
        loadIntent();
        setContentView(R.layout.activity_chat);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if(StringUtil.isNotNull(chatName)){
            toolbar.setTitle(chatName);
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //
        mListView = (ListView) findViewById(R.id.chat_listview);
        mEditText = (EditText) findViewById(R.id.et_message);
        mBtnSend = (Button) findViewById(R.id.btn_send_message);
        mBtnAudio = (Button) findViewById(R.id.btn_audio);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
        {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    void loadIntent(){
         Intent _mIm = getIntent();
        if(_mIm.hasExtra(CHAT_ID)){
            chatId = _mIm.getStringExtra(CHAT_ID);
        }
        if(_mIm.hasExtra(CHAT_NAME)){
            chatName = _mIm.getStringExtra(CHAT_NAME);
        }
    }


}
