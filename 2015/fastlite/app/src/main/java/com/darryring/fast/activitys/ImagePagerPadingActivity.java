package com.darryring.fast.activitys;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.darryring.fast.R;
import com.darryring.fast.adapter.ImagePagerAdapter;
import com.darryring.fast.base.BaseAppCompatActivity;
import com.darryring.fast.theme.FastTheme;
import com.darryring.libmodel.entity.theme.ThemeEntity;
import com.darryring.libview.PagerTitleSpotTabStrip;

import java.util.ArrayList;
import java.util.List;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class ImagePagerPadingActivity extends BaseAppCompatActivity {

    private String TAG="ImagePagerPadingActivity";
    ViewPager mViewPager;
    ListView mListView;
    PagerTitleSpotTabStrip mPagerTitleSpotTabStrip;
    ImagePagerAdapter mImagePagerAdapter;
    List<View> imageViews = new ArrayList<View>();

    List<String> imageUrls = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FastTheme.fastTheme.setupTheme(this, ThemeEntity.TYPE_NO_ACTION_BAR);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_pager);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //
        mListView = (ListView) findViewById(R.id.listview);
        View headerView = getLayoutInflater().inflate(R.layout.panel_image_pager_pading,mListView,false);
        mListView.addHeaderView(headerView);
        //
        mViewPager = (ViewPager) headerView.findViewById(R.id.image_viewpager);
        mPagerTitleSpotTabStrip = (PagerTitleSpotTabStrip) headerView.findViewById(R.id.pager_spot_strip);
        mListView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1,getData()));
        //
        imageUrls.add("http://img.zcool.cn/community/01d4d756e6c00232f875520fa3f04f.jpg@900w_1l_2o");
        imageUrls.add("http://img.zcool.cn/community/0197df569c435c32f87574be0acb1a.jpg@900w_1l_2o");
        imageUrls.add("http://img.zcool.cn/community/01d7be569c436032f87574be192fba.jpg@900w_1l_2o");
        imageUrls.add("http://img.zcool.cn/community/01a18e569c43676ac725af23f4066f.jpg@900w_1l_2o");

        imageUrls.add("http://img.zcool.cn/community/012876569c436e6ac725af233f88e3.jpg@900w_1l_2o");

        imageUrls.add("http://img.zcool.cn/community/019f70569c436f32f87574be5bdc2d.jpg@900w_1l_2o");
        imageUrls.add("http://img.zcool.cn/community/0169b9569c437b6ac725af23af5002.jpg@900w_1l_2o");
        imageUrls.add("http://img.zcool.cn/community/01bcde569c438d32f87574be94ef23.jpg@900w_1l_2o");
        imageUrls.add("http://img.zcool.cn/community/017cfe569c437c32f87574be3a3efd.jpg@900w_1l_2o");
        imageUrls.add("http://img.zcool.cn/community/0177c7569c43896ac725af237b3ab1.jpg@900w_1l_2o");
        imageViews.addAll(getImageViews());
        mImagePagerAdapter = new ImagePagerAdapter(imageViews,imageUrls);
        mViewPager.setOffscreenPageLimit(8);
        mViewPager.setAdapter(mImagePagerAdapter);
        mPagerTitleSpotTabStrip.setTabTextBackground(R.drawable.pager_spot_selector);
        mPagerTitleSpotTabStrip.setViewPager(mViewPager);
        mViewPager.setPageTransformer(true,new ZoomOutPageTransformer());
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
        {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class DepthPageTransformer implements ViewPager.PageTransformer {
        private  float MIN_SCALE = 0.75f;

        @SuppressLint("NewApi")
        @Override
        public void transformPage(View view, float position) {
            int pageWidth = view.getWidth();
            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setAlpha(0);
            } else if (position <= 0) { // [-1,0]
                // Use the default slide transition when
                // moving to the left page
                view.setAlpha(1);
                view.setTranslationX(0);
                view.setScaleX(1);
                view.setScaleY(1);
            } else if (position <= 1) { // (0,1]
                // Fade the page out.
                view.setAlpha(1 - position);
                // Counteract the default slide transition
                view.setTranslationX(pageWidth * -position);
                // Scale the page down (between MIN_SCALE and 1)
                float scaleFactor = MIN_SCALE + (1 - MIN_SCALE)
                        * (1 - Math.abs(position));
                view.setScaleX(scaleFactor);
                view.setScaleY(scaleFactor);
            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setAlpha(0);

            }
        }

    }
    //
    public class ZoomOutPageTransformer implements ViewPager.PageTransformer {
        private  float MIN_SCALE = 0.6f;

        private  float MIN_ALPHA = 0.5f;

        @Override
        public void transformPage(View view, float position) {
            int pageWidth = view.getWidth();
            int pageHeight = view.getHeight();

            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
//                view.setAlpha(0);
            } else if (position <= 1) { // [-1,1]
                // Modify the default slide transition to
                // shrink the page as well
                float scaleFactor = Math.max(MIN_SCALE, 1 - Math.abs(position));
                float vertMargin = pageHeight * (1 - scaleFactor) / 2;
                float horzMargin = pageWidth * (1 - scaleFactor) / 2;
                if (position < 0) {
                    view.setTranslationX(horzMargin - vertMargin / 2);
                } else {
                    view.setTranslationX(-horzMargin + vertMargin / 2);
                }
                // Scale the page down (between MIN_SCALE and 1)
                view.setScaleX(scaleFactor);
                view.setScaleY(scaleFactor);
                // Fade the page relative to its size.
                view.setAlpha(MIN_ALPHA + (scaleFactor - MIN_SCALE)
                        / (1 - MIN_SCALE) * (1 - MIN_ALPHA));
            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
//                view.setAlpha(0);
            }
        }
    }


    //
    private List<String> getData(){

        List<String> data = new ArrayList<String>();
        for(int i=0;i<100;i++) {
            data.add("Image"+(i+1));
        }
        return data;
    }
    private List<View> getImageViews()
    {
        List<View> _views = new ArrayList<View>();
        LayoutInflater inflater = getLayoutInflater();
        for(int i=0;i<10;i++) {
            View _newView = inflater.inflate(R.layout.item_image_pager,null);
            _views.add(_newView);
        }
        return _views;
    }


}
