package com.darryring.fast.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.darryring.fast.R;

/**
 * Created by hljdrl on 16/3/2.
 */
public class UserFragment extends BaseFragment implements AdapterView.OnItemClickListener {

    //
    Button btnAccountQrcode;
    ImageView imageViewUser;
    TextView tv_nick;
    TextView tv_account;
    String TAG = "UserFragment";
    public UserFragment() {
    }


    @SuppressLint("ValidFragment")
    public UserFragment(String _title) {
        setTitle(_title);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View _view = inflater.inflate(R.layout.fragment_user, container, false);
        return _view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View _view = getView();
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    }
}
