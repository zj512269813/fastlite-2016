package com.darryring.fast.activitys;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.darryring.fast.R;
import com.darryring.fast.adapter.ImageMultiPagerAdapter;
import com.darryring.fast.base.BaseAppCompatActivity;
import com.darryring.fast.theme.FastTheme;
import com.darryring.libmodel.entity.theme.ThemeEntity;
import com.darryring.libview.PagerTitleSpotTabStrip;
import com.pixplicity.multiviewpager.MultiViewPager;

import java.util.ArrayList;
import java.util.List;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class MultiViewPagerActivity extends BaseAppCompatActivity {

    private String TAG = "MultiViewPagerActivity";
    MultiViewPager mMultiViewPager;
    ListView mListView;
    PagerTitleSpotTabStrip mPagerTitleSpotTabStrip;
    ImageMultiPagerAdapter mImageMultiPagerAdapter;
    List<View> imageViews = new ArrayList<View>();

    List<String> imageUrls = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FastTheme.fastTheme.setupTheme(this, ThemeEntity.TYPE_NO_ACTION_BAR);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_multi_pager);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //
        mListView = (ListView) findViewById(R.id.listview);
        View headerView = getLayoutInflater().inflate(R.layout.panel_image_pager_multi, mListView, false);
        mListView.addHeaderView(headerView);
        //
        mMultiViewPager = (MultiViewPager) headerView.findViewById(R.id.pager);
        mPagerTitleSpotTabStrip = (PagerTitleSpotTabStrip) headerView.findViewById(R.id.pager_spot_strip);
        mListView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, getData()));
        //
        imageUrls.add("http://img.zcool.cn/community/01d4d756e6c00232f875520fa3f04f.jpg@900w_1l_2o");
        imageUrls.add("http://img.zcool.cn/community/0197df569c435c32f87574be0acb1a.jpg@900w_1l_2o");
        imageUrls.add("http://img.zcool.cn/community/01d7be569c436032f87574be192fba.jpg@900w_1l_2o");
        imageUrls.add("http://img.zcool.cn/community/01a18e569c43676ac725af23f4066f.jpg@900w_1l_2o");

        imageUrls.add("http://img.zcool.cn/community/012876569c436e6ac725af233f88e3.jpg@900w_1l_2o");

        imageUrls.add("http://img.zcool.cn/community/019f70569c436f32f87574be5bdc2d.jpg@900w_1l_2o");
        imageUrls.add("http://img.zcool.cn/community/0169b9569c437b6ac725af23af5002.jpg@900w_1l_2o");
        imageUrls.add("http://img.zcool.cn/community/01bcde569c438d32f87574be94ef23.jpg@900w_1l_2o");
        imageUrls.add("http://img.zcool.cn/community/017cfe569c437c32f87574be3a3efd.jpg@900w_1l_2o");
        imageUrls.add("http://img.zcool.cn/community/0177c7569c43896ac725af237b3ab1.jpg@900w_1l_2o");
        imageViews.addAll(getImageViews());
        mImageMultiPagerAdapter = new ImageMultiPagerAdapter(imageViews, imageUrls);
        mMultiViewPager.setAdapter(mImageMultiPagerAdapter);
        mPagerTitleSpotTabStrip.setTabTextBackground(R.drawable.pager_spot_selector);
        mPagerTitleSpotTabStrip.setViewPager(mMultiViewPager);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
        {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    //
    private List<String> getData() {

        List<String> data = new ArrayList<String>();
        for (int i = 0; i < 100; i++) {
            data.add("Image" + (i + 1));
        }
        return data;
    }

    private List<View> getImageViews() {
        List<View> _views = new ArrayList<View>();
        LayoutInflater inflater = getLayoutInflater();
        for (int i = 0; i < 10; i++) {
            View _newView = inflater.inflate(R.layout.item_image_multi_pager, null);
            _views.add(_newView);
        }
        return _views;
    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
