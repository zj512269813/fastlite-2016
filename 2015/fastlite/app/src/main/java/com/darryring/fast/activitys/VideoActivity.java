package com.darryring.fast.activitys;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.widget.MediaController;

import com.darryring.fast.R;
import com.darryring.libcore.Fast;
import com.darryring.libcore.android.media.UriType;
import com.darryring.libview.LibVideoView;

/**
 * Created by hljdrl on 15/12/26.
 */
public class VideoActivity extends Activity {


    LibVideoView mLibVideoView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        mLibVideoView = (LibVideoView) findViewById(R.id.libvideoview);
        String _assetsVideo = UriType.formatAssetsUri("media/big_buck_bunny.mp4");
//        String _assetsVideo = UriType.formatHttpUri("https://a1.easemob.com/iischool/ichat/chatfiles/ca233f60-e4fb-11e5-ac7a-6de559a5f872");
//        String _assetsVideo = UriType.formatAssetsUri("media/c46af5e0e4f611e5925203242a7fa871.amr");
        mLibVideoView.setMediaController(new MediaController(this));
        mLibVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                Fast.media.start();
            }
        });
        mLibVideoView.setVideoURI(_assetsVideo);
        mLibVideoView.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Fast.media.release();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
