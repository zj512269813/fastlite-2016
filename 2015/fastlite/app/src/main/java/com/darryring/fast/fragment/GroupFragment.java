package com.darryring.fast.fragment;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ListView;

import com.darryring.fast.R;
import com.darryring.fast.adapter.ListGradAdapter;
import com.darryring.fast.adapter.data.ItemData;
import com.darryring.libcore.Fast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by hljdrl on 16/3/2.
 */
public class GroupFragment extends BaseFragment{

    String TAG="GroupFragment";
    ListView mListview;
    public GroupFragment(){
    }
    @SuppressLint("ValidFragment")
    public GroupFragment(String _title){
        setTitle(_title);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View _view = inflater.inflate(R.layout.fragment_group,container,false);
        return _view;
    }
    ListGradAdapter mListGradAdapter;
    List<ItemData> mdata = new ArrayList<ItemData>();
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View _view = getView();
        mListview = (ListView) _view.findViewById(R.id.listview_user);
        mListGradAdapter = new ListGradAdapter(getActivity(),mdata);
        //
        mListview.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            public void onGlobalLayout() {

                final int width = mListview.getWidth();
                final int height = mListview.getHeight();
                Fast.logger.i(TAG,"Listview-Width: "+width);
                Fast.logger.i(TAG,"Listview-Height: "+height);
                mGridWidth = width;
                mGridHeight = height;

                final int desireSize = getResources().getDimensionPixelOffset(com.libtrace.imageselector.R.dimen.image_size);
                final int numCount = width / desireSize;
                final int columnSpace = getResources().getDimensionPixelOffset(com.libtrace.imageselector.R.dimen.space_size);
                int columnWidth = (width - columnSpace*(numCount-1)) / numCount;
                mListGradAdapter.setItemSize(columnWidth,numCount);
                mdata.clear();
                mdata.addAll(getData(numCount));
                mListview.setAdapter(mListGradAdapter);

                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
                    mListview.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }else{
                    mListview.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            }
        });
    }
    int mGridWidth;
    int mGridHeight;
    private static int getGroupSize()
    {
        int max=30;
        int min=1;
        Random random = new Random();
        int s = random.nextInt(max)%(max-min+1) + min;
        return s;
    }
    private List<ItemData> getData(final int numCount){

        List<ItemData> data = new ArrayList<ItemData>();
        List<String> group = new ArrayList<String>();
        group.add("A");
        group.add("B");
        group.add("C");
        group.add("D");
        group.add("E");
        group.add("F");
        group.add("G");
        group.add("H");
        group.add("I");
        group.add("J");
        group.add("K");
        for(String _tag:group) {

            List<String> _groupData = new ArrayList<String>();
            int _tagGroupSize = getGroupSize();
            for (int i = 0; i < _tagGroupSize; i++) {
                _groupData.add(_tag+String.valueOf(i));
                if(_groupData.size()>=numCount){
                    ItemData _itemData = new ItemData();
                    _itemData.setTag(_tag);
                    _itemData.setGridCount(_tagGroupSize);
                    List<String> _copylist = new ArrayList<String>();
                    _copylist.addAll(_groupData);
                    _groupData.clear();
                    _itemData.setData(_copylist);
                    data.add(_itemData);
                }
            }
            if(_groupData.size()>0){
                ItemData _itemData = new ItemData();
                _itemData.setGridCount(_tagGroupSize);
                _itemData.setTag(_tag);
                List<String> _copylist = new ArrayList<String>();
                _copylist.addAll(_groupData);
                _groupData.clear();
                _itemData.setData(_copylist);
                data.add(_itemData);
            }
        }
        return data;
    }
}
