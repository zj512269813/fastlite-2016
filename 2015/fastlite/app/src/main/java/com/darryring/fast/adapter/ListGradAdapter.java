package com.darryring.fast.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.darryring.fast.R;
import com.darryring.fast.adapter.data.ItemData;

import java.util.List;


/**
 * Created by hljdrl on 16/3/17.
 */
public class ListGradAdapter extends ArrayAdapter<ItemData> {
    LayoutInflater inflater;
    public ListGradAdapter(Context context , List<ItemData> objects) {
        super(context, 0, objects);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ItemData _item = getItem(position);
        ViewHolder _viewHolder;
        if(convertView==null){
            convertView = inflater.inflate(R.layout.item_list_grid,null,false);
            _viewHolder = new ViewHolder();
            _viewHolder.tag = (TextView) convertView.findViewById(R.id.item_tv_tag);
            _viewHolder.mViewGroup = (ViewGroup) convertView.findViewById(R.id.grid_layout);
            convertView.setTag(_viewHolder);
        }else{
            _viewHolder = (ViewHolder) convertView.getTag();
        }
        int _groupSize = _item.getData().size();
        LinearLayout _viewGroup = (LinearLayout) _viewHolder.mViewGroup;
        _viewGroup.setWeightSum(numCount);
        //
        //
        int _girdItemCount = _viewGroup.getChildCount();
        for(int i=0;i<_girdItemCount;i++) {
                if(i<_groupSize){
                    View _groupItemView = _viewGroup.getChildAt(i);
                    _groupItemView.setVisibility(View.VISIBLE);
                    _groupItemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Log.i("item", "OnClick()...");
                        }
                    });
                    TextView _textView = (TextView) _groupItemView;
                    _textView.setText(position+"-"+i);
                }else{
                    View _groupItemView = _viewGroup.getChildAt(i);
                    if(i<numCount){
                        _groupItemView.setVisibility(View.INVISIBLE);
                    }else{
                        _groupItemView.setVisibility(View.GONE);
                    }
                    _groupItemView.setOnClickListener(null);
                }
        }
        //
        if(position==0){
            _viewHolder.tag.setVisibility(View.VISIBLE);
            _viewHolder.tag.setText(_item.getTag()+" <---ItemPosition="+position+" -------GridCount="+_item.getGridCount());
        }else{
            ItemData _topItemData = getItem(position-1);
            if(_topItemData.getTag().equals(_item.getTag())){
                _viewHolder.tag.setVisibility(View.GONE);
                _viewHolder.tag.setText("");
            }else{
                _viewHolder.tag.setVisibility(View.VISIBLE);
                _viewHolder.tag.setText(_item.getTag()+" <---ItemPosition="+position+" -------GridCount="+_item.getGridCount());
            }

        }
        return convertView;
    }
    public static class ViewHolder{
        TextView tag;
        ViewGroup mViewGroup;
    }
    private int itemWidht;
    private int numCount;
    public void setItemSize(int itemSize,int _numCount){
        itemWidht  = itemSize;
        numCount = _numCount;
        //notifyDataSetChanged();
    }
}
