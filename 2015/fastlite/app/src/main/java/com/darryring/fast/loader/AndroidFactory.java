package com.darryring.fast.loader;

import android.app.Application;

import com.darryring.libcore.Fast;
import com.darryring.libcore.android.cache.FileDiskCache;
import com.darryring.libcore.android.cache.MemoryCache;
import com.darryring.libcore.android.file.AndroidFiles;
import com.darryring.libcore.android.http.AndroidOkHttp;
import com.darryring.libcore.android.logger.AndroidLogger;
import com.darryring.libcore.android.media.AndroidMedia;
import com.darryring.libcore.android.task.AndroidTask;
import com.darryring.libcore.android.util.AndroidNetWork;

/**
 * Created by hljdrl on 16/6/1.
 */
public final class AndroidFactory {

    public static void  newFastFactory (Application _app){
        if (Fast.logger == null) {
            Fast.logger = new AndroidLogger(true, true, true, true);
        }
        if (Fast.files == null) {
            Fast.files = new AndroidFiles();
        }
        if (Fast.task == null) {
            Fast.task = new AndroidTask();
        }
        if (Fast.http == null) {
            Fast.http = new AndroidOkHttp(_app);
        }
        if (Fast.kvCache == null) {
            Fast.kvCache = new MemoryCache();
        }
        if (Fast.diskCache == null) {
            Fast.diskCache = new FileDiskCache(_app);
        }
        if (Fast.media == null) {
            Fast.media = new AndroidMedia(_app);
        }
        if(Fast.network==null) {
            Fast.network = new AndroidNetWork(_app);
        }
    }
}
